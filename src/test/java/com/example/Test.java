package com.example;

import java.util.HashMap;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieRuntimeFactory;
import org.kie.dmn.api.core.DMNContext;
import org.kie.dmn.api.core.DMNModel;
import org.kie.dmn.api.core.DMNResult;
import org.kie.dmn.api.core.DMNRuntime;

public class Test {
    public static void main(String[] args) {
        KieServices ks = KieServices.Factory.get();
        KieContainer kcontainer = ks.getKieClasspathContainer();
        DMNRuntime dmnRuntime = KieRuntimeFactory.of(kcontainer.getKieBase()).get(DMNRuntime.class);
        //DMNModel dmnModel = dmnRuntime.getModel("https://kiegroup.org/dmn/_3735A8A1-5CBF-42FC-8B20-5005B5602708", "YearValidation");

        //DMNModel dmnModel = dmnRuntime.getModel("https://kiegroup.org/dmn/_47EDCA20-B751-4EC6-9E74-C8BD36CF2A94", "NameValidation");
        DMNModel dmnModel = dmnRuntime.getModel("https://kiegroup.org/dmn/_93BF7975-96F6-4B1B-805C-2264CDC0373C", "FinalValidation");
        DMNContext context = dmnRuntime.newContext();

        HashMap<String,Object> customer = new HashMap<String,Object>();

        customer.put("name", "Amiz");
        customer.put("year", 2012);

        context.set("Customer", customer);

        DMNResult result = dmnRuntime.evaluateAll(dmnModel, context);

        System.out.println(result.getDecisionResults());
    }   
}